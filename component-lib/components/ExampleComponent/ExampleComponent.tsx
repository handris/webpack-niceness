import React, { ReactElement } from 'react';
import { List } from 'antd';

import styles from './ExampleComponent.scss';

interface ISection {
  title: string;
  value?: string;
}

type returnType = ReactElement | null;

function ExampleComponent({ title, value }: ISection): returnType {
  if (value) {
    return (
      <List.Item className={styles.container}>
        <span>
          <b>{title}</b>
        </span>
        <span>{value}</span>
      </List.Item>
    );
  } else {
    return null;
  }
}

export default ExampleComponent;
