const path = require('path');
const webpack = require('webpack');
const pkg = require('./package.json');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const pkgName = pkg.name;

const componentsRoot = path.resolve(__dirname, './components');
const distDir = path.resolve(__dirname, 'dist');

const env = 'production';

module.exports = {
  entry: path.join(componentsRoot, 'index.tsx'),
  mode: env,
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
  ],
  externals: ['antd', 'react', 'moment'],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
          },
        ],
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { url: false, sourceMap: true, modules: true },
          },
          { loader: 'sass-loader', options: { sourceMap: true } },
        ],
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              [
                'import',
                {
                  libraryName: 'antd',
                  style: true, // or 'css'
                },
              ],
            ],
          },
        },
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.scss'],
  },
  output: {
    filename: 'index.js',
    path: distDir,
    libraryTarget: 'commonjs2',
    library: 'component-lib',
  },
};
